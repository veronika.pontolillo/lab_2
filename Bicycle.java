//Veronika Pontolillo 2034373
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        String str = "Manufacturer: "+this.manufacturer+", Number of Gears: "+this.numberGears+", MaxSpeed: "+this.maxSpeed;
        return str;
    }
}
